import { Component } from "react";

class UpDown extends Component {
     constructor(props) {
          super(props);
          this.state = {
               count: 0
          }
     }

     onBtnUp = () => {
          this.setState({
               count: this.state.count + 1
          })
     }

     onBtnDown = () => {
          this.setState({
               count: this.state.count - 1
          })
     }

     render() {
          return (
               <div>
                    <p>Count: {this.state.count}</p>
                    <button onClick={this.onBtnUp}>Up</button>
                    <button onClick={this.onBtnDown}>Down</button>
               </div>
          )
     }
}

export default UpDown;
